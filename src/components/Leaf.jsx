import React, {useState, useEffect} from 'react';
import { genHexString } from "../assets";


const Leaf = () => {
  const [bgColor, setBgColor] = useState("");

  useEffect(() => {
    setBgColor(genHexString(6))
  }, [])

  return (
    <div className="leaf item-style" style={{backgroundColor: `#${bgColor}`}}>
      Leaf
    </div>
  );
};

export default Leaf;