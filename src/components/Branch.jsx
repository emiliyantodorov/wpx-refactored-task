import React, { useState, useEffect } from 'react';
import { genHexString } from "../assets";
import Leaf from "./Leaf";

const Branch = ({ branch }) => {
  const [ bgColor, setBgColor ] = useState("");
  const [ curBranch, setCurBranch ] = useState(branch);

  useEffect(() => {
    setBgColor(genHexString(6));
  }, []);

  const changeInputHandler = evt => {
    setBgColor(evt.target.value);
  };

  const addBranch = () => {
    setCurBranch(prevState => {
      const updatedBranches = [ ...prevState.branches, {
        id: Math.random(),
        branches: [],
        leaves: [],
      } ];

      return { ...prevState, branches: updatedBranches };
    });
  };

  const addLeaf = () => {
    setCurBranch(prevState => {
      const updatedLeaves = [ ...prevState.leaves, {
        id: Math.random(),
        branches: [],
        leaves: [],
      } ];

      return { ...prevState, leaves: updatedLeaves };
    });
  };

  return (
    <>
      <div className="branch item-style"
           style={{ backgroundColor: `#${bgColor}`}}
      >
        <span>Branch</span>
        <span>(B {curBranch.branches.length})</span>
        <span>(L {curBranch.leaves.length})</span>
        <form style={{ marginBottom: "1rem" }}>
          <input onChange={changeInputHandler} value={bgColor} type="text" id="color" name="color"
                 placeholder="cccccc"/>
          <div>
            <button type="button" onClick={addBranch}>Add Branch</button>
            <button type="button" onClick={addLeaf}>Add Leaf</button>
          </div>
        </form>

        {
          curBranch.branches.map(curBranch => {

              return <Branch
                key={curBranch.id}
                branch={curBranch}
              />;
            }
          )
        }
        {
          curBranch.leaves.map(curLeaf => <Leaf key={curLeaf.id}/>)
        }
      </div>
    </>
  );
};

export default Branch;