import React, { useState } from 'react';
import Branch from "./components/Branch";
import "./App.css";

const App = () => {

  const [ mainBranch, setMainBranch ] = useState({
    id: 0,
    branches: [],
    leaves: [],
  });

  return (
    <div>
      {
        <Branch
          branch={mainBranch}
        />
      }
    </div>
  );
};

export default App;